/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokemonultrared;

import java.io.Serializable;



/**
 *
 * @author Anthony Loor
 */
public class Entrenador implements Serializable{
    private Mochila mochila;
    private String nombre;
    private int dinero;
    
    public Entrenador(Mochila mochila, String nombre, int dinero){
        this.mochila=mochila;
        this.nombre=nombre;
        this.dinero=dinero;
    }
    
    public Mochila getMochila(){
        return this.mochila;
    }
    public String getNombre(){
        return this.nombre;
    }
    public int getDinero(){
        return this.dinero;
    }
}
