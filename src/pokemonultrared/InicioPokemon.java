/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokemonultrared;

import java.util.Scanner;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

/**
 *
 * @author PC-1
 */
public class InicioPokemon {
    private Image bgImage = new Image("file:src/red-blue/Start_background.png");
    private VBox root=new VBox();
    private HBox paneNombre=new HBox();
    private Label lblNombre=new Label("Nombre de entrenador Pokemon:    ");
    private Label es1=new Label("\n Elige tu Pokemon inicial:");
    private Label es2=new Label("");
    private TextField txtNombre = new TextField();
    private HBox paneBotones = new HBox();
    private Button btnBulbasaur= new Button("Bulbasaur");
    private Button btnCharmander= new Button("Charmander");
    private Button btnSquirtle= new Button("Squirtle");
    private Button btnContinuar= new Button("Continuar");
    private StackPane container = new StackPane();
    
    public InicioPokemon(){
        organizarElementos();
        crearJugador();
    }
    
    public void organizarElementos(){
        ImageView imageView = new ImageView();
        imageView.setImage(bgImage);
        container.getChildren().add(imageView);
        btnBulbasaur.setGraphic(new ImageView("/red-blue/red-blue/1.png"));
        btnCharmander.setGraphic(new ImageView("/red-blue/red-blue/4.png"));
        btnSquirtle.setGraphic(new ImageView("/red-blue/red-blue/7.png"));
        root.getChildren().addAll(paneNombre, es1, paneBotones, es2, btnContinuar);
        paneNombre.getChildren().addAll(lblNombre, txtNombre);
        paneBotones.getChildren().addAll(btnBulbasaur, btnCharmander, btnSquirtle);
        root.setAlignment(Pos.CENTER);
        paneNombre.setAlignment(Pos.CENTER);
        paneBotones.setAlignment(Pos.CENTER);
        container.getChildren().add(root);
        
    }
    
    public void crearJugador(){
        String nombre = String.valueOf(txtNombre.getCharacters());
        
    }
    public StackPane getRoot(){
        return this.container;
    }
}
