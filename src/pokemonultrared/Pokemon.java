/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokemonultrared;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Anthony Loor
 */
public class Pokemon {
    private List<String> habilidades= new ArrayList<>();
    private List<String> multiplicador = new ArrayList<>();//cuando se use el multiplicador debe pasarse a float.
    private int ataque;
    private int captureRate;
    private int defensa;
    private int hp;
    private int hpCompleta;
    private String nombre;
    private int pokedexIndex;
    private String tipo;
    private int generación;
    private int isLegendary;
    private String rutaImagenFront;
    private String rutaImagenBack;
    
    public Pokemon(List<String> habilidades, List<String> multiplicador, int ataque, int captureRate, int defensa, int hp, int hpCompleta, String nombre, int pokedexIndex, String tipo, int generación, int isLegendary, String rutaImagenFront, String rutaImagenBack){
        this.habilidades=habilidades;
        this.multiplicador=multiplicador;
        this.ataque=ataque;
        this.captureRate=captureRate;
        this.defensa=defensa;
        this.hp=hp;
        this.hpCompleta=hpCompleta;
        this.nombre=nombre;
        this.pokedexIndex=pokedexIndex;
        this.tipo=tipo;
        this.generación=generación;
        this.isLegendary=isLegendary;
        this.rutaImagenFront=rutaImagenFront;
        this.rutaImagenBack=rutaImagenBack;
    }
    
    public int isLegendary(){
        return this.isLegendary;
    }
    public List<String> getHabilidades(){
        return this.habilidades;
    }
    public List<String> getMultiplicador(){
        return this.multiplicador;
    }
    public int getAtaque(){
        return this.ataque;
    }
    public int getCaptureRate(){
        return this.captureRate;
    } 
    public int getDefensa(){
        return this.defensa;
    }
    public int getHP(){
        return this.hp;
    }

    public void setHP(int hpCompleta) {
        this.hp = hpCompleta;
    }

    public int getHpCompleta() {
        return hpCompleta;
    }
    
    public String getNombre(){
        return this.nombre;
    }
    public int getPokedexIndex(){
        return this.pokedexIndex;
    }
    public String getTipo(){
        return this.tipo;
    }
    public int getGeneracion(){
        return this.generación;
    }
    public String getRutaImagenFront(){
        return this.rutaImagenFront;
    }
    public String getRutaImagenBack(){
        return this.rutaImagenBack;
    }
}
