/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokemonultrared;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Anthony Loor
 */
public class Villa {
    private Gimnasio gimnasio;
    private Hospital hospital;
    public List<Entrenador> entrenadores= new ArrayList<>();
    public List<Arbusto> arbustos= new ArrayList<>();
    
    public Villa(Gimnasio gimnasio, Hospital hospital, List<Entrenador> entrenadores, List<Arbusto> arbustos){
        this.gimnasio=gimnasio;
        this.hospital=hospital;
        this.entrenadores=entrenadores;
        this.arbustos=arbustos;
    }
}
