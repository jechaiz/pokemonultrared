/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokemonultrared;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 * @author Julian Echaiz
 */
public class PokemonUltraRed extends Application {
    Pokedex pkdx;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch();
    }
    
    public void start(Stage stage){
        Scene scene = new Scene(new InicioPokemon().getRoot());
        stage.setScene(scene);
        stage.setTitle("POKEMON ULTRA RED");
        stage.setMinHeight(720);
        stage.setMinWidth(1280);
        stage.show();
    }
    
    
    public void agregarPokemons() throws IOException{
        List<Pokemon> pokemons = new ArrayList<>();
        List<String> linea = Files.readAllLines(Paths.get("/src/Pokedesc/pokemon.csv"));
        for(String l: linea){
            String [] s = l.split(",");
            List<String> habilidades = new ArrayList<>();
            String [] h = s[0].substring(1, s[0].length()).split(",");
            for(String ss: h){
                habilidades.add(ss);
            }    
            List<String> multiplicador = new ArrayList<>();
            for(int i=1; i<19;i++){
                multiplicador.add(s[i]);
            }
            int ataque = Integer.parseInt(s[19]);
            int captureRate = Integer.parseInt(s[20]);
            int defensa = Integer.parseInt(s[21]);
            int hp = Integer.parseInt(s[22]);
            int hpCompleta = Integer.parseInt(s[22]);
            String nombre = s[23];
            int pokedexIndex = Integer.parseInt(s[24]);
            String tipo = s[25];
            int generacion = Integer.parseInt(s[26]);
            int isLegendary = Integer.parseInt(s[27]);
            String rutaImagenFront = "/src/red-blue/red-blue/"+String.valueOf(pokedexIndex)+".png";
            String rutaImagenBack = "/src/red-blue/red-blue/back/"+String.valueOf(pokedexIndex)+".png";
            Pokemon p = new Pokemon(habilidades, multiplicador, ataque, captureRate, defensa, hp, hpCompleta, nombre, pokedexIndex, tipo, generacion, isLegendary, rutaImagenFront, rutaImagenBack);
            pokemons.add(p);
        }
        pkdx = new Pokedex(pokemons);
    }
}
